package com.palominolabs.jackson.datatype.trove.ser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import gnu.trove.map.hash.TIntObjectHashMap;

public class TIntObjectMapSerializerTest extends BaseTroveSerializerTest {

    @Test
    public void testSimple() throws IOException
    {
        TIntObjectHashMap<String> map = new TIntObjectHashMap<String>();
        map.put(13, "foo");

        assertEquals("{\"13\":\"foo\"}", getString(writer, map));
    }
}
